<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/','HomeController@index');


Route::resource('audioprzewodnik','Language\LanguageController');


Route::resource('audio_guide','Admin\AudioGuideController');
Route::resource('audio_guide_point','Admin\AudioGuidePointController');

Route::resource('code','Admin\CodeController');
Route::resource('lang','Admin\LangController');

Route::get('/queue-add','HomeController@test');

Route::get('audio_guide/{id}', 'Admin\AudioGuideController@export')->name('audio_guide.export');

Route::get('audio_guide/{id}/open', 'Admin\AudioGuideController@open')->name('audio_guide.open');

Route::get('audio_guide/{id}/clear', 'Admin\AudioGuideController@clear')->name('audio_guide.clear');

Route::get('audio_guide_point/{id}/', 'Admin\AudioGuidePointController@mass')->name('audio_guide_point.mass');


Route::get('/queue-check', function () {
    //
});
