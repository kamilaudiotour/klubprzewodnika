<?php


namespace AudiotourCMS\Http\Controllers\Admin;


use AudiotourCMS\Http\Controllers\Controller;
use AudiotourCMS\Http\Models\Admin\AudioGuide;
use AudiotourCMS\Http\Models\Admin\Lang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $items = Lang::getAllRecords();
        return view('admin.lang.list')->with(['items'=>$items]);
    }

    public function create(Request $request)
    {
        return view('admin.lang.add');
    }

    public function store(Request $request)
    {
//        $items = Lang::getAllRecords();
        $store = Lang::storeRecord($request);

        foreach($request->all() as $key=>$value){
            $request->request->remove($key);
        }
        self::update($request,$store->id);

//        return view('admin.lang.list')->with(['items'=>$items]);
        return redirect(route('audio_guide.index'));
    }

    public function show($id)
    {
        $item = Lang::getRecord($id);
        return view('admin.lang.show')->with(['item'=>$item]);
//        $items = Code::getAllArchiveRecords();
//        return view('admin.code.show')->with(['items'=>$items]);
    }

    public function edit($id)
    {
//        $items = Code::getAllRecordsToArchive();
//        foreach ($items as $item){
//            DB::update("update code set visible = '0' where id = :id", ['id' => $item->id]);
//        }
//
//        return redirect(route('code.index'));
    }

    public function update(Request $request, $id)
    {
        if ($request->file('icon')) {
            $file = $request->file('icon');
            $storage = $file->store('public/lang/icons');
            $name_file = explode('/', $storage);
            $banner = '/storage/lang/icons/'. $name_file[3];
        }else{
            $banner = '';
        }

//        $items = Lang::getAllRecords();
        Lang::updateRecord($request,$id,$banner);
//        return view('admin.lang.list')->with(['items'=>$items]);
        return redirect(route('audio_guide.index'));
    }

    public function destroy($id)
    {
        Lang::destroyRecord($id);
        return redirect(route('audio_guide.index'));
    }
}
