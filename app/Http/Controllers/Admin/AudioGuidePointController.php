<?php

namespace AudiotourCMS\Http\Controllers\Admin;

use AudiotourCMS\Http\Controllers\Controller;
use AudiotourCMS\Http\Models\Admin\AudioGuide;
use AudiotourCMS\Http\Models\Admin\AudioGuidePoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AudioGuidePointController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        echo "dskm";
    }

    public function create(Request $reuqest)
    {
        $lang_id = $reuqest->about_park_id;
        $points = AudioGuidePoint::getPointsByID1($lang_id);
        $lang = AudioGuide::getRecord($lang_id);
        return view('admin.audio_guide.point-add')->with(['lang'=>$lang, 'points'=>$points]);
    }

    public function store(Request $request)
    {
        $store = AudioGuidePoint::storeRecord($request);

        foreach($request->all() as $key=>$value){
            $request->request->remove($key);
        }

        $request->request->add(['id_article' => $store->id]);
        $request->request->add(['id_category' =>$store->id_category]);

        self::update($request,$store->id);
        return redirect(route('audio_guide.index'));
    }

    public function show($id)
    {
        $item = AudioGuidePoint::getRecord($id);
        $points = AudioGuidePoint::getPointsByID1($item->id_category);
        return view('admin.audio_guide.point-show')->with(['item'=>$item, 'points'=>$points]);
    }

    public function edit($id)
    {
        $points = AudioGuidePoint::getPointsByID1($id);
        return view('admin.audio_guide.add-all')->with(['audio_guide_id'=>$id, 'points'=>$points]);
    }

    public function update(Request $request, $id)
    {
        if ($request->file('banner')) {
            $file = $request->file('banner');
            $storage = $file->store('public/audio_guide/'.$request->id_category.'/points/'.$id.'/banners');
            $name_file = explode('/', $storage);
            $banner = '/storage/audio_guide/'.$request->id_category.'/points/'.$id.'/banners/'. $name_file[6];
        }else{
            $banner = '';
        }

        if ($request->file('audio_file')) {
            $file = $request->file('audio_file');
            $storage = $file->store('public/audio_guide/'.$request->id_category.'/points/'.$id.'/audio_file');
            $name_file = explode('/', $storage);
            $audio_file = '/storage/audio_guide/'.$request->id_category.'/points/'.$id.'/audio_file/'. $name_file[6];
        }else{
            $audio_file = '';
        }

        if ($request->file('video_file')) {
            $file = $request->file('video_file');
            $storage = $file->store('public/audio_guide/'.$request->id_category.'/points/'.$id.'/video_file');
            $name_file = explode('/', $storage);
            $video_file = '/storage/audio_guide/'.$request->id_category.'/points/'.$id.'/video_file/'. $name_file[6];
        }else{
            $video_file = '';
        }

        AudioGuidePoint::updateRecord($request,$id,$banner,$audio_file,$video_file);

        AudioGuide::updatePointsArray($request->id_article,$request->id_category);

        return redirect(route('audio_guide.index'));
    }

    public function destroy($id)
    {
        AudioGuidePoint::destroyRecord($id);
        return redirect(route('audio_guide.index'));
    }

    public function export(Request $request, $id)
    {
        echo $request->audio_guide_id;

        if ($request->file('images_new')){
            foreach($request->file('images_new') as $file){
                $file = $file;
                $filename = "";
                $name = $file->getClientOriginalName();
                if (strlen($name) > 1) {
                    if (substr($name, 0, 1) === "0") {
                        $filename = substr($file->getClientOriginalName(), 1);
                    }else {
                        $filename = $file->getClientOriginalName();
                    }
                }else {
                    $filename = $file->getClientOriginalName();
                }
                $from = preg_replace('/\.[^.]+$/','',$filename);

                if ($request->has('pointID'.$from)){
                    $storage = $file->storeAs('public/audio_guide/'.$request->audio_guide_id.'/points/'.$request->get('pointID'.$from).'/banners', $from.'.jpeg');
                    explode('/', $storage);

                    $banner = '/storage/audio_guide/'.$request->audio_guide_id.'/points/'.$request->get('pointID'.$from).'/banners/'. $from.'.jpeg';
                    DB::update("update audio_guide_point set banner = '$banner'where id = :id", ['id' => $request->get('pointID'.$from)]);
                }
            }
        }
        if ($request->file('audio_new')) {
            foreach($request->file('audio_new') as $file){
                $file = $file;
                $filename = "";
                $name = $file->getClientOriginalName();
                if (strlen($name) > 1) {
                    if (substr($name, 0, 1) === "0") {
                        $filename = substr($file->getClientOriginalName(), 1);
                    }else {
                        $filename = $file->getClientOriginalName();
                    }
                }else {
                    $filename = $file->getClientOriginalName();
                }
                $from = preg_replace('/\.[^.]+$/','',$filename);

                if ($request->has('pointID'.$from)){
                    $storage = $file->storeAs('public/audio_guide/'.$request->audio_guide_id.'/points/'.$request->get('pointID'.$from).'/audio_file', $from.'.mpga');
                    explode('/', $storage);

                    $banner = '/storage/audio_guide/'.$request->audio_guide_id.'/points/'.$request->get('pointID'.$from).'/audio_file/'. $from.'.mpga';
                    DB::update("update audio_guide_point set audio_file = '$banner'where id = :id", ['id' => $request->get('pointID'.$from)]);
                }
            }
        }

        $items = AudioGuide::getAllRecords(3, 1);
        return view('admin.audio_guide.list')->with(['items'=>$items]);
    }
}
