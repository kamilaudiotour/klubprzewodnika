<?php

namespace AudiotourCMS\Http\Controllers\Admin;

use AudiotourCMS\Http\Controllers\Controller;
use AudiotourCMS\Http\Models\Admin\AudioGuide;
use AudiotourCMS\Http\Models\Admin\AudioGuidePoint;
use Illuminate\Http\Request;
use AudiotourCMS\Http\Models\Admin\Lang;
use Response;

class AudioGuideController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $items = AudioGuide::getAllRecords(3, 1);
        return view('admin.audio_guide.list')->with(['items'=>$items]);
    }

    public function create()
    {
        $langs = Lang::getAllRecords();
        return view('admin.audio_guide.add')->with(['langs'=>$langs]);
    }

    public function store(Request $request)
    {
        $store = AudioGuide::storeRecord($request);

        foreach($request->all() as $key=>$value){
            $request->request->remove($key);
        }
        self::update($request,$store->id);

        return redirect(route('audio_guide.index'));
    }

    public function show($id)
    {
        $langs = Lang::getAllRecords();
        $audio_guides = AudioGuide::getRecord($id);
        return view('admin.audio_guide.show')->with(['item'=>$audio_guides,'langs'=>$langs]);
    }

    public function edit($id)
    {
        $column = array();
        array_push($column, 'ID' );
        array_push($column, 'Nazwa' );
        array_push($column, 'Kolejnosc' );
        array_push($column, 'Dodane Wideo' );
        array_push($column, 'Nadrzedne' );
        array_push($column, 'Podrzedne' );
        array_push($column, 'Wybrano' );
        array_push($column, 'Wysluchano' );


        $table = AudioGuidePoint::getPointsByID1($id);

        $handle = fopen("/var/www/gdynia/storage/app/public/audio_guide/gdynia.csv", 'w+');
//        $handle = fopen("/Applications/XAMPP/htdocs/welski/app/Http/Controllers/Admin/gdynia.csv", 'w+');


        fputcsv($handle, $column, ";",'"');

        foreach($table as $row) {
            $a = array();

            array_push($a, $row['id'] );
            array_push($a, $row['name'] );
            // array_push($a, iconv('UTF-8', 'Windows-1250', $row['name']) );
            array_push($a, $row['position'] );
            array_push($a, $row['is_video'] );
            array_push($a, $row['parent'] );
            array_push($a, $row['is_child'] );
            array_push($a, $row['is_select'] );
            array_push($a, $row['was_heard'] );

            fputcsv($handle, $a, ";",'"');
        }

        fclose($handle);

        //        //server
        return Response::download("/var/www/gdynia/storage/app/public/audio_guide/gdynia.csv", "gdynia.csv");
//        return Response::download("/Applications/XAMPP/htdocs/welski/app/Http/Controllers/Admin/gdynia.csv", "gdynia.csv");
    }

    public function open($id)
    {
        $item = AudioGuide::getRecord($id);
        return view('admin.audio_guide.export')->with(['item'=>$item]);
    }

    public function clear($id)
    {
        AudioGuidePoint::deleteAllByTrackID($id);
        $items = AudioGuide::getAllRecords(3, 1);
        return view('admin.audio_guide.list')->with(['items'=>$items]);
    }

    public function export(Request $request, $id)
    {
        if ($request->file('import_svc_file')){
            $file_path = $request->file('import_svc_file');

            $file = fopen($file_path, 'r');

            $count = 0;

            while (($line = fgetcsv($file)) !== FALSE) {    // Read one line
                $item = str_replace(' "', '', $line);
                $item2 = str_replace('" ', '', $item);
                $item3 = str_replace(null, ' ', $item2);
                $array[] = $item3;
                $count ++;
            }

            $columns = explode(';', $array[0][0]);

            $back = "http://127.0.0.1:8000/audio_guide";

            if ($request->checkbox_add && $columns[0] == "ID"){
                echo "<h4 style='color: red;' ><b>Chcesz dodać punkty ale nie usuńełęś kolumne ID</b></h4>";
                return;
            }
            if ($request->checkbox_update && $columns[0] != "ID"){
                echo "<h4 style='color: red;' ><b>Chcesz zaaktualizować punkty ale nie ma w pliku kolumny ID</b></h4>";
                return;
            }


            if ($request->checkbox_add){
                echo "<h4 style='color: red;' ><b>Dodałeś te punkty</b></h4>";
            }
            if ($request->checkbox_update){
                echo "<h4><b>Zaaktualizowałeś te punkty</b></h4>";
            }

            echo "<a style='font-size: 30px' href='$back'>Wróć</a>";

            echo "<table style='border: 1px solid black;border-collapse: collapse;width:100%;text-align: left'>";
            foreach (range(0,$count-1) as $i){
                if ($i > 0){
                    $a = explode(';', $array[$i][0]);
                    $countValue = 0;
                    echo '<tr style="border: 1px solid black;">';

                    $id_point = $a[0];
                    $data = $request->all();

                    foreach ($a as $value){

                        if($columns[$countValue] == "ID"){
                            print "<th style='padding: 7px;'> ".$columns[$countValue].": ".$value."</th>";
                        }
                        if($columns[$countValue] == "Nazwa") {
                            $strName = iconv('Windows-1250', 'UTF-8', $value);
                            print "<th> " . $columns[$countValue] . ": " . $strName . "</th>";
                            $data['name'] = $strName;
                        }
                        if($columns[$countValue] == "Kolejnosc"){
                            print "<th> ".$columns[$countValue].": ".$value."</th>";
                            $data['position'] = $value;
                        }

                        if($columns[$countValue] == "Dodane Wideo"){
                            print "<th> ".$columns[$countValue].": ".$value."</th>";
                            $data['is_video'] = $value;
                        }

                        if($columns[$countValue] == "Nadrzedne"){
                            print "<th> ".$columns[$countValue].": ".$value."</th>";
                            $data['parent'] = $value;
                        }
                        if($columns[$countValue] == "Podrzedne"){
                            print "<th> ".$columns[$countValue].": ".$value."</th>";
                            $data['is_child'] = $value;
                        }

                        $countValue++;
                    }

                    if ($request->checkbox_add){
                        $data['id_category'] = $id;

                        $store = AudioGuidePoint::create($data);

                        $update = $request->all();
                        $update['id_article'] = $store->id;
                        AudioGuidePoint::find($store->id)->update($update);
                    }
                    if ($request->checkbox_update){
                        $data['id_article'] = $id_point;
                        AudioGuidePoint::find($id_point)->update($data);
                    }

                    echo '</tr>';
                }
            }
            echo "</table>";


            $back = "http://127.0.0.1:8000/audio_guide";

            if ($request->checkbox_add){
                echo "<h4 style='color: red;' ><b>Dodałeś te punkty</b></h4>";
            }
            if ($request->checkbox_update){
                echo "<h4><b>Zaaktualizowałeś te punkty</b></h4>";
            }

            echo "<a style='font-size: 30px' href='$back'>Wróć</a>";
            fclose($file);
        }
    }

    public function update(Request $request, $id)
    {
        AudioGuide::updateRecord($request,$id);
        return redirect(route('audio_guide.index'));
    }

    public function destroy($id)
    {
        AudioGuide::destroyRecord($id);
        return redirect(route('audio_guide.index'));
    }
}
