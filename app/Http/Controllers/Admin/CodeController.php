<?php

namespace AudiotourCMS\Http\Controllers\Admin;

use AudiotourCMS\Http\Controllers\Controller;
use AudiotourCMS\Http\Models\Admin\Code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $items = Code::getAllRecords();
        return view('admin.code.list')->with(['items'=>$items]);
    }

    public function create(Request $request)
    {
        $added = 0;
        for ($i = 1; $i <= 200; $i++) {
            $id = rand(99999,999999);
            if (Code::find($id)){
            }else{
                $data = $request->all();
                $data['id'] = $id;
                $data['code'] = $id;
                $data['visible'] = "1";
                $data['used'] = "0";
                $data['create'] = Carbon::now('Europe/Paris')->toDateTimeString();
                $data['timestamp'] = "";
                Code::create($data);
                $added += 1;
            }
           if ($added == 1){
               return redirect(route('code.index'));
           }
        }
        return redirect(route('code.index'));
    }

    public function store(Request $request)
    {
        echo "store";
    }

    public function show($id)
    {
        $items = Code::getAllArchiveRecords();
        return view('admin.code.show')->with(['items'=>$items]);
    }

    public function edit($id)
    {
        $items = Code::getAllRecordsToArchive();
        foreach ($items as $item){
            DB::update("update code set visible = '0' where id = :id", ['id' => $item->id]);
        }

        return redirect(route('code.index'));
    }

    public function update(Request $request, $id)
    {
        echo "update";
    }

    public function destroy($id)
    {
        $items = Code::getAllArchiveRecordsToDelete();
        foreach ($items as $item){
            Code::find($item->id)->delete();
//            DB::update("update code set visible = '1' where id = :id", ['id' => $item->id]);
        }
        $items1 = Code::getAllArchiveRecords();
        return view('admin.code.show')->with(['items'=>$items1]);
    }
}
