<?php


namespace AudiotourCMS\Http\Controllers\Language;


use AudiotourCMS\Http\Controllers\Controller;
use AudiotourCMS\Http\Models\Admin\AudioGuide;
use AudiotourCMS\Http\Models\Admin\AudioGuidePoint;
use AudiotourCMS\Http\Models\Admin\Code;
use AudiotourCMS\Http\Models\Admin\Lang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {
        $items = AudioGuide::getAllRecords(100, 0);
        return view('audioprzewodnik.start')->with(['items'=>$items]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        $item = AudioGuide::getRecord($id);
        $items = AudioGuidePoint::getPointsByID0($id);
        if (Session::get('userCODE')){

            $count = $item['is_select'];
            $count += 1;
            DB::update("update audio_guide set is_select = '$count' where id = :id", ['id' => $item['id']]);

            return view('audioprzewodnik.list')->with(['lang'=>$item->tag, 'id'=>$item->id, 'items'=>$items]);
        }else{
            return view('audioprzewodnik.code')->with(['lang'=>$item->tag, 'id'=>$item->id]);
        }
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $item = AudioGuide::getRecord($id);
        $items = AudioGuidePoint::getPointsByID0($id);
        if (strlen($request->code) >= 6){
            $code = Code::getRecord($request->code);
                if ($code['code'] == null){
                    echo "Nie istnieje takiego kodu";
                }else{
                    if ($code['visible'] == 1){
                        $time = Carbon::now('Europe/Paris')->toDateTimeString();
                        $code1 = Code::getRecord($request->code);

                        $start = date_create($time);
                        $end = date_create($code1['create']);
                        $diff = date_diff($end,$start);
//                        echo $diff->format('%d hours');
                        if ($diff->format('%D') == 00){
                            $count = $code1['used'];
                            $count += 1;
                            DB::update("update code set used = '$count' where id = :id", ['id' => $code['id']]);
                            DB::update("update code set timestamp = '$time' where id = :id", ['id' => $code['id']]);
                            Session::put('userCODE', $code['code']);

                            $count = $item['is_select'];
                            $count += 1;
                            DB::update("update audio_guide set is_select = '$count' where id = :id", ['id' => $item['id']]);

                            return view('audioprzewodnik.list')->with(['lang'=>$item->tag, 'id'=>$item->id, 'items'=>$items]);
                        }else{
                            echo "Mineło 24 godziny";
                        }
                    }else{
                        echo "Wpisałeś stary kod dostępu";
                    }
                }
        }else{
            echo "Niepoprawny kod";
        }
    }

    public function destroy($id)
    {

    }
}
