<?php


namespace AudiotourCMS\Http\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{
    protected $fillable = ['id', 'name', 'icon', 'tag'];
    public $timestamps = false;
    protected $table = 'lang';

    public static function getAllRecords(){
        $result = Lang::select('*');

        $result = $result->orderBy('id','desc')->paginate(50);

        return $result;
    }

    public static function getRecord($id){
        return Lang::find($id);
    }

    public static function updateRecord($request,$id, $banner){
        $data = $request->all();

        unset($data['_token']);
        unset($data['_method']);

        if ($banner != ''){
            $data['icon'] = $banner;
        }

        Lang::find($id)->update($data);
    }

    public static function storeRecord($request){
        $data = $request->all();
        unset($data['_token']);
        return Lang::create($data);
    }

    public static function destroyRecord($id){
        Lang::find($id)->delete();
    }
}