<?php

namespace AudiotourCMS\Http\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AudioGuidePoint extends Model
{
    protected $fillable = [
      'banner',
      'name',
      'position',
      'video_file',
      'is_video',
      'id_article',
      'id_category',
      'audio_file',
      'parent',
      'is_child',
      'visible',
      'is_select',
      'was_heard'
    ];
    public $timestamps = false;
    protected $table = 'audio_guide_point';

    public static function getPointsByID1($id){
        return AudioGuidePoint::where('id_category',$id)->orderBy('position','ASC')->get()->toArray();
    }
    public static function getPointsByID0($id){
        return AudioGuidePoint::where('id_category',$id)->orderBy('position','ASC')->where('visible', 1)->get()->toArray();
    }

    public static function getRecord($id){
        return AudioGuidePoint::find($id);
    }

    public static function storeRecord($request){

        $data = $request->all();
        unset($data['_token']);
        $data['id_category'] = $request->audio_guide_id;

        return AudioGuidePoint::create($data);
    }

    public static function updateRecord($request, $id, $banner,$audio_file,$video_file){

        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);

        if ($banner != ''){
            $data['banner'] = $banner;
        }

        if ($audio_file != ''){
            $data['audio_file'] = $audio_file;
        }

        if ($video_file != ''){
            $data['video_file'] = $video_file;
        }

        if (isset($request->id_article)){
            $data['id_article'] = $request->id_article;
        }

        if (isset($request->id_category)){
            $data['id_category'] = $request->id_category;
        }

        AudioGuidePoint::find($id)->update($data);
    }

    public static function destroyRecord($id){
        AudioGuidePoint::find($id)->delete();
    }

    public static function deleteAllByTrackID($id){
        $result = AudioGuidePoint::where('id_category',$id)->delete();
        return $result;
    }
}
