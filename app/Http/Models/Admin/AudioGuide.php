<?php

namespace AudiotourCMS\Http\Models\Admin;

use AudiotourCMS\Http\Controllers\Admin\AudioGuidePointController;
use Illuminate\Database\Eloquent\Model;

class AudioGuide extends Model
{
    protected $fillable = ['name', 'position', 'timestamp', 'pointArray', 'flag', 'visible', 'is_select', 'tag'];
    public $timestamps = false;
    protected $table = 'audio_guide';

    public static function getAllRecords($paginate, $visible){

        if ($visible == 1){
            $result = AudioGuide::select('*');
        }else{
            $result = AudioGuide::where('visible', 1);
        }

        $result = $result->orderBy('position','ASC')->paginate($paginate);

        foreach($result as $key=>$item){
            if ($visible == 0){
                $pointsByIds = AudioGuidePoint::getPointsByID0($item->id);
            }else{
                $pointsByIds = AudioGuidePoint::getPointsByID1($item->id);
            }
            $item->pointArray = $pointsByIds;
        }
        return $result;
    }

    public static function getRecord($id){
        return AudioGuide::find($id);
    }

    public static function updateRecord($request,$id){
        $data = $request->all();

        unset($data['_token']);
        unset($data['_method']);

        AudioGuide::find($id)->update($data);
    }

    public static function updatePointsArray($point_id, $id_category){
        $record = AudioGuide::whereId($id_category);

        $get_data = $record->get();
        $points = json_decode($get_data[0]->pointArray);

        $points[] = $point_id;

        $data = ['pointArray'=> json_encode($points)];
        $record->update($data);
    }

    public static function storeRecord($request){
        $data = $request->all();
        unset($data['_token']);
        $store = AudioGuide::create($data);
        return $store ;
    }

    public static function destroyRecord($id){
        AudioGuide::find($id)->delete();
    }
}
