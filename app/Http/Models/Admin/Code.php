<?php

namespace AudiotourCMS\Http\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $fillable = ['id', 'visible', 'used', 'code', 'create', 'timestamp'];
    public $timestamps = false;
    protected $table = 'code';

    public static function getAllRecords(){
        $result = Code::select('*')->where('visible',1);

        $result = $result->orderBy('create','desc')->paginate(50);

        return $result;
    }
    public static function getAllRecordsToArchive(){
        $result = Code::select('*')->where('visible',1);

        $result = $result->paginate(5000000);

        return $result;
    }

    public static function getAllArchiveRecords(){
        $result = Code::select('*')->where('visible',0);

        $result = $result->paginate(50);

        return $result;
    }
    public static function getAllArchiveRecordsToDelete(){
        $result = Code::select('*')->where('visible',0);
        $result = $result->paginate(5000000);
        return $result;
    }


    public static function getRecord($id){
        return Code::find($id);
    }

    public static function updateRecord($request,$id){
        $data = $request->all();

        unset($data['_token']);
        unset($data['_method']);
        Code::find($id)->update($data);
    }

    public static function storeRecord($request){
        $data = $request->all();
        unset($data['_token']);
        return Code::create($data);
    }

    public static function destroyRecord($id){
        Code::find($id)->delete();
    }
}
