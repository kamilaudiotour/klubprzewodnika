<?php

namespace AudiotourCMS\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
use AudiotourCMS\Http\Models\Admin\Street;
use DB;
use AudiotourCMS\Http\Models\Admin\OneSignal;
use Log;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->call(function () {
		 $streets = Street::select('streets.name', 'streets.date', 'streets.categories', 'streets.id', 'streets.title', 'streets.subtitle', 'streets.body',DB::raw('group_concat(uts.uuid)as uuids'))->whereRaw("date >= NOW() and date <= DATE_ADD(NOW(), INTERVAL 3 DAY)")
		->leftjoin('user_to_street as uts','uts.street_id','streets.id')->groupBy('streets.id')
		->get()->toArray();

		foreach($streets as $index=>$value){
			$res = OneSignal::sendNotificationUUID($value['subtitle'].' - '.$value['title'],$value['subtitle'],$value['body'],$value['uuids']);
			Log::info("-------------------START-------------------");
			Log::info('Schedule Push Street: '.Carbon::now().' - UUID: '.$value['uuids']);
			Log::info('Responce: '.$res );
			Log::info("-------------------END---------------------");
		} 
			
		});
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
