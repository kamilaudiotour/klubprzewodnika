@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row header">
			<div class="col-md-12 border-bottom" >
				<div class="col-md-12 title">
					<h2>KODY DOSTĘPU</h2>
				</div>
				<div class="col-md-12" >
					<a href="{{ route('code.destroy',1) }}" onclick="event.preventDefault(); document.getElementById('delete-form-{{1}}').submit();" class="col-md-2" style="padding-left: 0px"><i style="color:red" class="fa fa-trash "></i> </a>
					<form id="delete-form-{{1}}" action="{{ route('code.destroy',1) }}" method="POST" style="display: none;">
						<input type="hidden" name="_method" value="DELETE" />
						@csrf
					</form>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="row">
			<div class="col-md-12">
				<ul style="list-style: none; padding: 0!important;">
					@foreach($items as $item)

						<li style="height: 15px;">
							<p class="col-md-2">Kod: <b>{{$item->id}}</b></p>
							<p class="col-md-3">Dodany: <b>{{$item->create}}</b></p>
							<p class="col-md-5">Logowania: <b>@if($item->used > 0) {{$item->used}} @else Niema @endif</b></p>

							<p class="col-md-2">Dodany w archive: <b>@if($item->visible == 0) Tak @else Nie @endif</b></p>
						</li>

						<hr>
					@endforeach
				</ul>
			</div>
		</div>
		{{$items->links()}}
	</div>
@stop
