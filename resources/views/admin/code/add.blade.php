@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="{{route('code.store')}}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}


				<ul class="nav md-pills pills-secondary">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#panelpl" role="tab">Polski</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#panelen" role="tab">English</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#panelde" role="tab">Deutsch</a>
					</li>
				</ul>

				<div class="tab-content pt-0">
					<div class="tab-pane fade in show active" id="panelpl" role="tabpanel">
						<br>
						<div class="form-group row">
							<label class="col-md-1 col-form-label">Tytuł</label>
							<div class="col-md-11">
								<input type="text" name="name_pl" value="" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="panelen" role="tabpanel">
						<br>
						<div class="form-group row">
							<label class="col-md-1 col-form-label">Titul</label>
							<div class="col-md-11">
								<input type="text" name="name_en" value="" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="panelde" role="tabpanel">
						<br>
						<div class="form-group row">
							<label class="col-md-1 col-form-label">Titel</label>
							<div class="col-md-11">
								<input type="text" name="name_de" value="" class="form-control"/>
							</div>
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group row" >
					<label class="col-md-2 col-form-label">Opublikowany</label>
					<div class="col-md-10">
						<select class="form-control" name="status" >
							<option value="1">Tak</option>
							<option value="0">Nie</option>
						</select>
					</div>
				</div>

				<hr>

				<div class="form-group row" >
					<label class="col-md-2 col-form-label">Kolejność</label>
					<div class="col-md-10">
						<input type="text" name="position" value="" class="form-control"/>
					</div>
				</div>

				<hr>

				<div class="form-group row" >
					<label class="col-md-2 col-form-label">Ikona w aplikcji</label>
					<div class="col-md-10">
						<select class="form-control" name="icon" >

								<option style="background: #ccc" value="home">Strona Główna</option>
								<option style="background: #ccc" value="mapa">Mapa</option>
								<option style="background: #ccc" value="bazanoclegowa">Baza Noclegowa</option>
								<option style="background: #ccc" value="edukacja">Edukacja</option>
								<option style="background: #ccc" value="fauna">Fauna</option>
								<option style="background: #ccc" value="flora">Flora</option>
								<option style="background: #ccc" value="geocaching">Geo Caching</option>
								<option style="background: #ccc" value="gleby">Gleby</option>
								<option style="background: #ccc" value="historia">Historia</option>
								<option style="background: #ccc" value="klimat">Klimat</option>
								<option style="background: #ccc" value="muzeumetnograficzne">Muzeum Etnograficzne</option>
								<option style="background: #ccc" value="muzeumprzyrody">Muzeum Przyrody</option>
								<option style="background: #ccc" value="ofertaedukacyjna">Oferta Edukacyjna</option>
								<option style="background: #ccc" value="oparku">O parku</option>
								<option style="background: #ccc" value="parkwliczbach">Park w liczbach</option>
								<option style="background: #ccc" value="płazyigady">Płazy i gady</option>
								<option style="background: #ccc" value="podzielsię">Podziel się</option>
								<option style="background: #ccc" value="położenieparku">Położenie parku</option>
								<option style="background: #ccc" value="pomnikiprzyrody">Pomniki Przyrody</option>
								<option style="background: #ccc" value="powrót">Powrót</option>
								<option style="background: #ccc" value="przyroda">Przyroda</option>
								<option style="background: #ccc" value="ptaki">Ptaki</option>
								<option style="background: #ccc" value="questy">Questy</option>
								<option style="background: #ccc" value="rezerwatprzyrody">Rezerwat Przyrody</option>
								<option style="background: #ccc" value="ryby">Ryby</option>
								<option style="background: #ccc" value="skanerkodówQR">Skaner kodów QR</option>
								<option style="background: #ccc" value="ssaki">Ssaki</option>
								<option style="background: #ccc" value="szlakipiesze">Szlaki Piesze</option>
								<option style="background: #ccc" value="szlakirowerowe">Szlaki Rowerowe</option>
								<option style="background: #ccc" value="szlakkajakowy">Szlak Kajakowy</option>
								<option style="background: #ccc" value="turystyka">Turystyka</option>
								<option style="background: #ccc" value="ukrztałtowanieterenu">Ukrztałtowanie terenu</option>
								<option style="background: #ccc" value="użytkiekologiczne">Użytki ekologiczne</option>
								<option style="background: #ccc" value="zabytki">Zabytki</option>
							<!-- <option style="background: #ccc" value="home">Strona Główna</option>
							<option style="background: #ccc" value="oparku">O parku</option>
							<option style="background: #ccc" value="przyroda">Przyroda</option>
							<option style="background: #ccc" value="dziedzictwo">Dziedzictwo kulturowe</option>
							<option style="background: #ccc" value="szlaki">Szlaki i ścieżki turystyczne</option>
							<option style="background: #ccc" value="zagospodarowanie">Zagospodarowanie turystyczne</option>
							<option style="background: #ccc" value="mapa">Mapa</option>
							<option style="background: #ccc" value="planner">Planner</option>
							<option style="background: #ccc" value="oaplikacji">O Aplikacji</option>

							<option style="background: #b17575" value="kontakt">Kontakt</option>
							<option style="background: #b17575" value="dlazwiedzajacych">Dla zwiedzających</option>
							<option style="background: #b17575" value="siedzibaparku">Siedziba parku</option>
							<option style="background: #b17575" value="parkwpigulce">Park w pigułce</option>

							<option style="background: #65bcc4" value="puszczaromincka">Puszcza Romincka</option>
							<option style="background: #65bcc4" value="rzezbaterenu">Rzeźba terenu</option>
							<option style="background: #65bcc4" value="jeziorairzeki">Jeziora i rzeki</option>
							<option style="background: #65bcc4" value="rosliny">Rośliny</option>
							<option style="background: #65bcc4" value="zwierzeta">Zwierzęta</option>
							<option style="background: #65bcc4" value="grzybyiporosty">Grzyby i porosty</option>
							<option style="background: #65bcc4" value="rezerwatyprzyrody">Rezerwat przyrody</option>
							<option style="background: #65bcc4" value="pomnikiprzyrody">Pomniki pszyrody</option>
							<option style="background: #65bcc4" value="obszarnatura2000">Obszar Natura 2000</option>

							<option style="background: #d0cf6c" value="historia">Historia</option>
							<option style="background: #d0cf6c" value="dawnetorowisko">Dawne torowisko</option>
							<option style="background: #d0cf6c" value="pamiatkoweglazy">Pamiątkowe głazy</option>
							<option style="background: #d0cf6c" value="palaceiparki">Pałace i parki</option>
							<option style="background: #d0cf6c" value="zabytkowekoscioly">Zabytkowe kościoły</option>
							<option style="background: #d0cf6c" value="cmentarzeimiejscapamieci">Cmentarze i miejsca pamięci</option>
							<option style="background: #d0cf6c" value="inneobjekty">Inne objekty</option>

							<option style="background: #7eac72" value="szlakipiesze">Szlaki piesze</option>
							<option style="background: #7eac72" value="szlakirowerowe">Szlaki rowerowe</option>
							<option style="background: #7eac72" value="sciezkiedukacyjne">Ścieżki edukacyjne</option>
							<option style="background: #7eac72" value="ogrodkidydaktyczne">Ogródki dydaktyczne</option> -->
						</select>
					</div>
				</div>

				<hr>

				<input type="submit" value="Dodaj" class="btn btn-primary custom-btn">
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#sub_title_pl').summernote();
	$('#sub_title_cz').summernote();

	$('.data_od,.data_do').datepicker({
		dateFormat: 'yyyy-mm-dd',
		timepicker: 'hh:i'
	})

	$(document).on({
		dragover: function() {
			return false;
		},
		drop: function() {
			return false;
		}
	});
</script>
@stop
