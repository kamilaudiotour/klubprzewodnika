@extends('layouts.app')

@section('content')
<div class="container">
<div class="row header">
	<div class="col-md-12 border-bottom" >
		<div class="col-md-12 title">
			<h2>KODY DOSTĘPU</h2>
		</div>
		<div class="col-md-12" >
			<a href="{{route('code.create')}}" class="btn btn-primary m-b-20 custom-btn"><i class="fa fa-plus"></i> Dodaj kod</a>
			<a href="{{route('code.edit', 0)}}" class="btn btn-primary m-b-20 custom-delete-btn"><i class="fa fa-trash"></i> Zarchiwizuj wszystkie</a>
			<a href="{{route('code.show', 0)}}" class="btn btn-primary m-b-20 custom-edit-btn"><i class="fa fa-eye"></i> Archive</a>
		</div>
	</div>
</div>
	<br>
	<br>
<div class="row">
	<div class="col-md-12">
		<ul style="list-style: none; padding: 0!important;">
			@foreach($items as $item)
				<li style="height: 15px;">
					<p class="col-md-2">Kod: <b>{{$item->id}}</b></p>
					<p class="col-md-3">Dodany: <b>{{$item->create}}</b></p>
					<p class="col-md-3">Status: <b>@if(date_diff(date_create($item->create),date_create(Carbon\Carbon::now('Europe/Paris')->toDateTimeString()))->format('%D') == 00) Ważny @else Wygasł @endif</b></p>
					<p class="col-md-2">Logowania: <b>@if($item->used > 0) {{$item->used}} @else Niema @endif</b></p>
				</li>
					<hr>
			@endforeach
		</ul>
	</div>
</div>
	{{$items->links()}}
</div>
@stop
