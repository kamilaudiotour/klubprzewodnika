@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="{{route('lang.store')}}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}

				<h2>Nowy język</h2>

				<hr>

				<div class="form-group row">
					<label class="col-md-2 col-form-label">Język</label>
					<div class="col-md-10">
						<input type="text" name="name" value="" class="form-control"/>
					</div>
				</div>

				<hr>
				<div class="form-group row">
					<div class="col-md-12">
						<h4 style="color: red">Ikona powinna być w wymiarach <b>177 x 134</b> w formacie <b>.png</b></h4>
					</div>

					<label class="col-md-2 col-form-label">Banner</label>
					<div class="col-md-10">
						<input type="file" name="icon"/>
						<img v-if="url" :src="url" />
					</div>
				</div>

				<hr>

				<div class="form-group row">
					<label class="col-md-2 col-form-label">Skrót (pl, en, de)</label>
					<div class="col-md-10">
						<input type="text" name="tag" value="" class="form-control"/>
					</div>
				</div>

				<hr>

				<input type="submit" value="Dodaj" class="btn btn-primary custom-btn">
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#sub_title_pl').summernote();
	$('#sub_title_cz').summernote();

	$('.data_od,.data_do').datepicker({
		dateFormat: 'yyyy-mm-dd',
		timepicker: 'hh:i'
	})

	$(document).on({
		dragover: function() {
			return false;
		},
		drop: function() {
			return false;
		}
	});
</script>
@stop
