@extends('layouts.app')

@section('content')
<div class="container">
<div class="row header">
	<div class="col-md-12 border-bottom" >
		<div class="col-md-12 title">
			<h2>JĘZYKI LISTA</h2>
		</div>
		<div class="col-md-12" >
			<a href="{{route('lang.create')}}" class="btn btn-primary m-b-20 custom-btn"><i class="fa fa-plus"></i> Dodaj język</a>
		</div>
	</div>
</div>
	<br>
	<br>
<div class="row">
	<div class="col-md-12">
		<ul style="list-style: none; padding: 0!important;">
			@foreach($items as $item)
				<li style="height: 15px;display: flex;">
					<img style="height: 50px;margin-top: -12px;" src="{{$item->icon}}" alt="flag">
					<p style="margin-top: 4px;" class="col-md-3">Język: <b>{{$item->name}}</b></p>

					<a style="height: 30px;margin-top: -2px;" href="{{route('lang.show',$item->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edytuj język</a>
					<a style="height: 30px;margin-top: -2px;" class="btn btn-danger custom-delete-btn" href="{{ route('lang.destroy',$item->id) }}" onclick="event.preventDefault(); document.getElementById('delete-form-item-{{$item->id}}').submit();"><i class="fa fa-trash-o"></i> Usuń język</a>

					<form id="delete-form-item-{{$item->id}}" action="{{ route('lang.destroy',$item->id) }}" method="POST" style="display: none;">
						<input type="hidden" name="_method" value="DELETE" />
						@csrf
					</form>
				</li>
				<hr>
			@endforeach
		</ul>
	</div>
</div>
	{{$items->links()}}
</div>
@stop
