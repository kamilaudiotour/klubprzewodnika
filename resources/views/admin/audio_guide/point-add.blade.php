@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
<div class="col-md-12"  id="app">
<form action="{{route('audio_guide_point.store')}}" method="POST" enctype="multipart/form-data">

<input type="hidden" name="audio_guide_id" value="{{$lang->id}}" />
{{csrf_field()}}

	<h2>Nowy punkt. Język: {{$lang->name}}</h2>

	<hr>

	<div class="form-group row">
		<label class="col-md-2 col-form-label">Nazwa: </label>
		<div class="col-md-10">
			<input type="text" name="name" value="" class="form-control"/>
		</div>
	</div>

	<hr>

	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Kolejność: </label>
		<div class="col-md-2">
			<select class="form-control" name="position" >
				@foreach(range(0,500) as $i)
					<option value="{{$i}}">{{$i}}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="form-group row">
		<label class="col-md-2 col-form-label">Visible: </label>
		<div class="col-md-3">
			<select class="form-control" name="visible" >
				<option value="1">Tak</option>
				<option value="0">Nie</option>
			</select>
		</div>
	</div>

	<hr>

	<div class="form-group row">
		<div class="col-md-12">
			<h4 style="color: red">Zdjęcia muszą być z rozdzielczością nie wększą niż <b>480 x 640</b> w formacie <b>.jpg</b></h4>
		</div>

		<label class="col-md-2 col-form-label">Banner</label>
		<div class="col-md-10">
			<input type="file" name="banner"/>
			<img v-if="url" :src="url" />
		</div>
	</div>

	<hr>

	<div class="form-group row">
		<div class="col-md-12">
			<h4 style="color: red">Pliki audio w formacie <b>.mp3</b></h4>
		</div>

		<label class="col-md-2 col-form-label">Audio Plik</label>
		<div class="col-md-10">
			<input type="file" accept="audio/mp3" name="audio_file" />
		</div>
	</div>

	<hr>
	<div class="form-group row">
		<div class="col-md-12">
			<h4 style="color: red">Pliki video w formacie <b>.mp4</b></h4>
		</div>

		<label class="col-md-2 col-form-label">Video Plik</label>
		<div class="col-md-10">
			<input type="file" name="video_file"/>
		</div>
	</div>
	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Odtwarzaj video: </label>
		<div class="col-md-2">
			<select class="form-control" name="is_video" >
				<option value="0">Nie</option>
				<option value="1">Tak</option>
			</select>
		</div>
	</div>
	<hr>


	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Czy ma w sobie punkty pozszezone: </label>
		<div class="col-md-2">
			<select class="form-control" name="is_child" >
				<option value="0">Nie</option>
				<option value="1">Tak</option>
			</select>
		</div>
	</div>
	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Punkt nadrzędny: </label>
		<div class="col-md-2">
			<select class="form-control" name="parent" >
				<option value="">-- Wybierz--</option>
				@foreach($points as $item)
					@if($item['is_child'] == "1")
						<option value="{{$item['id']}}">{{$item['name']}}</option>@endif
				@endforeach
			</select>
		</div>
	</div>

	<hr>

	<input type="submit" value="Dodaj" class="btn btn-primary custom-btn">
</form>



</div>
</div>
</div>
<script type="application/javascript">
	$(document).on({
		dragover: function() {
			return false;
		},
		drop: function() {
			return false;
		}
	});

</script>
@stop
