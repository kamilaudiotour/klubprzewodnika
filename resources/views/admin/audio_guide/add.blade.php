@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="{{route('audio_guide.store')}}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}

				<h2>Nowy język</h2>

				<hr>

				<div class="form-group row">
					<label class="col-md-2 col-form-label">Nazwa: </label>
					<div class="col-md-10">
						<input type="text" name="name" value="" class="form-control"/>
					</div>
				</div>

				<hr>

				<div class="form-group row" >
					<label class="col-md-2 col-form-label">Kolejność: </label>
					<div class="col-md-2">
						<select class="form-control" name="position" >
							<option value="0">-- Wybierz --</option>
							@foreach(range(1,100) as $i)
								<option value="{{$i}}">{{$i}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-2 col-form-label">Visible: </label>
					<div class="col-md-3">
						<select class="form-control" name="visible" >
							<option value="1">Tak</option>
							<option value="0">Nie</option>
						</select>
					</div>
				</div>

				<hr>

				<div class="form-group row">
					<label class="col-md-2 col-form-label">Flaga: </label>
					<div class="col-md-3">
						<select class="form-control" name="flag" >
							<option value="0">----------------wybierz----------------</option>
							@foreach($langs as $item)
								<option value="{{$item->icon}}">{{$item->name}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<hr>

				<div class="form-group row" style="display: none" >
					<label class="col-md-2 col-form-label">Data dodania</label>
					<div class="col-md-10">
						<input type="text" name="timestamp" value="{{\Carbon\Carbon::now('Europe/Paris')->toDateTimeString()}}" class="form-control"/>
					</div>
				</div>

				<input type="submit" value="Dodaj" class="btn btn-primary custom-btn">
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#summernote').summernote();

	$('.data_od,.data_do').datepicker({
		dateFormat: 'yyyy-mm-dd',
		timepicker: 'hh:i'
	})

	$(document).on({
		dragover: function() {
			return false;
		},
		drop: function() {
			return false;
		}
	});
</script>
@stop
