@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row header">
            <div class="col-md-12 border-bottom">
                <div class="col-md-12 title">
                    <h2>Masowe Dodawanie zdjęć i nagrań</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"  id="app">
                <form action="{{route('audio_guide_point.export', $audio_guide_id)}}" method="POST" enctype="multipart/form-data">

                    <input type="hidden" name="audio_guide_id" value="{{$audio_guide_id}}" />
                    {{csrf_field()}}

                    <h3 style="color: red">UWAGA! Język / Ścieżka ma {{count($points)}} punktów. o nazwach: <br> @foreach($points as $item) {{$item['position']}},@endforeach</h3>
                    <br>
                    <input type="hidden" name="size" value="{{count($points)}}" />
                    @foreach($points as $item)
                        <input type="hidden" name="pointID{{$item['position']}}" value="{{$item['id']}}" />
                    @endforeach

                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">Dodaj wszystkie Zdjęcia (.jpg 480 x 640)</label>
                        <div class="col-md-7">
                            <input type="file" accept="image/jpeg" name="images_new[]" value=""  multiple/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">Dodaj wszystkie Nagrania (tylko .mp3)</label>
                        <div class="col-md-7">
                            <input type="file" accept="audio/mp3" name="audio_new[]" multiple/>
                        </div>
                    </div>

                    <input type="submit" value="Dodaj" class="btn btn-primary">
                </form>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3 style="color: red">Zdjęcia muszą być z rozdzielczością nie wększą niż <b>480 x 640</b> w formacie .jpg</h3>
                <h3 style="color: red">Nagrania wrzucamy skompresowane z odpowiednimi parametrami tylko w <b>formacie .mp3</b></h3>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on({
            dragover: function() {
                return false;
            },
            drop: function() {
                return false;
            }
        });
    </script>
@stop
