@extends('layouts.app')

@section('content')
<div class="container">
<div class="row header">
	<div class="col-md-12 border-bottom" >
		<div class="col-md-12 title">
			<h2>AUDIOPRZEWODNIK - LISTA</h2>
		</div>
		<div class="col-md-12" >
			<a href="{{route('audio_guide.create')}}" class="btn btn-primary m-b-20 custom-btn"><i class="fa fa-plus"></i> Dodaj nowy język</a>
		</div>
	</div>
</div>
<br>
<br>
@foreach($items as $item)
		<div class="col-md-4 custom-app-card">
			<div class="custom-app-wrap">
				<ul style="text-align: center">
					<li><h4 style="text-align: left;">Statystyki</h4></li>
					<li><p style="text-align: left; font-size: 12px;"><b>Wybrano: </b>{{$item->is_select}}</p></li>
					<hr>
					<li><p style="text-align: left; font-size: 9px;"><b>id: </b>{{$item->id}}</p></li>
							<li><img src="{{$item->flag}}" alt="{{$item->name}}" class="show-blade-image m-b-20"/></li>
					<li><h5><b>Język: </b>{{$item->name}}</h5></li>
					<li><h5><b>Kolejność: </b>{{$item->position}}</h5></li>
					<hr>
					<a href="{{route('audio_guide.edit',$item->id)}}" class="btn btn-primary custom-edit-btn">Export</a>
					<a href="{{route('audio_guide.open',$item->id)}}" class="btn btn-primary custom-edit-btn">Import</a>
					<a href="{{route('audio_guide.clear',$item->id)}}" class="btn btn-primary custom-delete-btn">Usuń wszystko</a>
				</ul>

				<ul>
				@foreach($item->pointArray as $point)
					<li style="height: 25px;">
						<p>@if($point['is_video'] == 1) v @endif</p>
						<a href="{{route('audio_guide_point.show',$point['id'])}}" style="padding-left: 0px; @if($point['parent'] != null || $point['parent'] != 0 ) margin-left: 10px; @endif"   class="col-md-9"> <span style="color: red">{{$point['position']}}||</span> @if($point['is_child'] == 1) &#8595 @endif  @if($point['parent'] != null || $point['parent'] != 0 ) {{$point['parent']}} &#8594 @endif {{$point['id']}}. @if(strlen($point['name']) > 30) {{mb_substr($point['name'], 0, 20)}} ... @else {{$point['name']}} @endif</a>
						<a href="{{route('audio_guide_point.show',$point['id'])}}" style="padding-left: 0px"class="col-md-1"><i  class="fa fa-edit "></i> </a>
						<a href="{{ route('audio_guide_point.destroy',$point['id']) }}" onclick="event.preventDefault(); document.getElementById('delete-form-{{$point['id']}}').submit();" class="col-md-1" style="padding-left: 0px"><i style="color:red" class="fa fa-trash "></i> </a>
						<form id="delete-form-{{$point['id']}}" action="{{ route('audio_guide_point.destroy',$point['id']) }}" method="POST" style="display: none;">
							<input type="hidden" name="_method" value="DELETE" />
							@csrf
						</form>
					</li>
				@endforeach
					<li style="margin-top: 10px"><a href="{{route('audio_guide_point.create', ['about_park_id'=>$item->id])}}" class="btn btn-primary m-b-20 custom-btn"><i class="fa fa-plus"></i> Dodaj nowy punkt</a></li>
					<li style="margin-top: 10px"><a href="{{route('audio_guide_point.edit', $item->id)}}" class="btn btn-primary m-b-20 custom-btn"><i class="fa fa-plus"></i> Dodaj masowo</a></li>
				</ul>

				<hr>

				<ul style="display: contents;">
					<a href="{{route('audio_guide.show',$item->id)}}" class="btn btn-primary custom-edit-btn"><i class="fa fa-edit"></i> Edytuj język</a>
					<a class="btn btn-danger custom-delete-btn" href="{{ route('audio_guide.destroy',$item->id) }}" onclick="event.preventDefault(); document.getElementById('delete-form-item-{{$item->id}}').submit();"><i class="fa fa-trash-o"></i> Usuń język</a>

					<form id="delete-form-item-{{$item->id}}" action="{{ route('audio_guide.destroy',$item->id) }}" method="POST" style="display: none;">
						<input type="hidden" name="_method" value="DELETE" />
						@csrf
					</form>
				</ul>

			</div>
		</div>
@endforeach
{{$items->links()}}
</div>
@stop
