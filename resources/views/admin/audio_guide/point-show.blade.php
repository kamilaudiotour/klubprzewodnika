@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
<div class="col-md-12">
<form action="{{route('audio_guide_point.update',$item->id)}}" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT" />

<input type="hidden" name="id_category" value="{{$item->id_category}}" />
{{csrf_field()}}

	<h3>Statystyki</h3>
	<h4>Wybrano: {{$item->is_select}}</h4>
	<h4>Wysłuchano: {{$item->was_heard}}</h4>

	<hr>

	<h2>Edytuj: {{$item->name}}</h2>


	<hr>

	<div class="form-group row">
		<label class="col-md-2 col-form-label">Nazwa: </label>
		<div class="col-md-10">
			<input type="text" name="name" value="{{$item->name}}" class="form-control"/>
		</div>
	</div>

	<hr>

	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Kolejność: </label>
		<div class="col-md-2">
			<select class="form-control" name="position" >
				<option value="0">-- Wybierz --</option>
				@foreach(range(0,100) as $i)
					<option value="{{$i}}" @if($i == $item->position) selected @else @endif>{{$i}}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="form-group row">
		<label class="col-md-2 col-form-label">Visible: </label>
		<div class="col-md-3">
			<select class="form-control" name="visible" >
				<option value="1" @if($item->visible == 1) selected @endif>Tak</option>
				<option value="0" @if($item->visible == 0) selected @endif>Nie</option>
			</select>
		</div>
	</div>

	<hr>

	<div class="form-group row">
		<div class="col-md-12">
			<h4 style="color: red">Zdjęcia muszą być z rozdzielczością nie wększą niż <b>480 x 640</b> w formacie <b>.jpg</b></h4>
		</div>

		<label class="col-md-2 col-form-label">Banner</label>
		<div class="col-md-10">
			<input type="file" name="banner"/>
			<img v-if="url" :src="url" />
			@if($item->banner != null)
				<img src="{{$item->banner}}" alt="{{$item->name}}">
			@endif
		</div>
	</div>

	<hr>

	<div class="form-group row">
		<div class="col-md-12">
			<h4 style="color: red">Pliki audio w formacie <b>.mp3</b></h4>
		</div>

		<label class="col-md-2 col-form-label">Audio Plik</label>
		<div class="col-md-10">
			<input type="file" accept="audio/mp3" name="audio_file" />
			@if($item->audio_file != null)
				<audio controls>
					<source src="{{$item->audio_file}}" type="audio/ogg">
					<source src="{{$item->audio_file}}" type="audio/mpeg">
				Your browser does not support the audio element.
				</audio>
			@endif
		</div>
	</div>

	<hr>
	<div class="form-group row">
		<div class="col-md-12">
			<h4 style="color: red">Pliki video w formacie <b>.mp4</b></h4>
		</div>

		<label class="col-md-2 col-form-label">Video Plik</label>
		<div class="col-md-10">
			<input type="file" name="video_file"/>
			@if($item->video_file != null)
				<video width="320" height="240" controls>
					<source src="{{$item->video_file}}" type="video/mp4">
					<source src="{{$item->video_file}}" type="video/ogg">
					Your browser does not support the video tag.
				</video>
			@endif
		</div>
	</div>
	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Odtwarzaj video: </label>
		<div class="col-md-2">
			<select class="form-control" name="is_video" >
				<option value="0" @if($item->is_video == "0") selected @else @endif>Nie</option>
				<option value="1" @if($item->is_video == "1") selected @else @endif>Tak</option>
			</select>
		</div>
	</div>
	<hr>

	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Czy ma w sobie punkty pozszezone: </label>
		<div class="col-md-2">
			<select class="form-control" name="is_child" >
				<option value="0" @if($item->is_child == "0") selected @else @endif>Nie</option>
				<option value="1" @if($item->is_child == "1") selected @else @endif>Tak</option>
			</select>
		</div>
	</div>

	<div class="form-group row" >
		<label class="col-md-2 col-form-label">Punkt nadrzędny: </label>
		<div class="col-md-2">
			<select class="form-control" name="parent" >
				<option value="">-- Wybierz --</option>
				@foreach($points as $point)
					@if($point['is_child'] == "1")
						<option value="{{$point['id']}}" @if($item->parent == $point['id']) selected @else @endif>{{$point['name']}}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>

	<hr>
	<input type="submit" value="Zapisz" class="btn btn-primary custom-btn">
</form>
</div>
</div>
</div>
        <script type="text/javascript">
			$('#desc_pl').summernote();
			$('#desc_en').summernote();
			$('#desc_de').summernote();

		   $('.data_od,.data_do').datepicker({
				dateFormat: 'yyyy-mm-dd',
				timepicker: 'hh:i'
			})

        </script>
@stop
