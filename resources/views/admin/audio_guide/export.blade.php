@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row header">
            <div class="col-md-12 border-bottom">
                <div class="col-md-12 title">
                    <h4>IMPORTUJ DO</h4>
                    <h2>{{$item->name}}</h2>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="container">
        <div class="row header">
            <div class="col-md-12 border-bottom">
                <form action="{{route('audio_guide.export', $item->id)}}" method="POST" enctype="multipart/form-data">

                    {{csrf_field()}}
                    <input style="display: none" type="text" name="id_svc" value="{{$item->id}}" class="form-control"/>

                    <p style="color: red">Dodaj plik w <b>formacie .svc</b></p>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Plik .svc</label>
                        <div class="col-md-9">
                            <input type="file" name="import_svc_file" />
                        </div>
                    </div>

                    <h3 style="color: red">Możesz wybrać tylko jedną opcje</h3>

                    <h4 style="color: red">Gdy dodajeś nowe punkty trzeba usunąć kolumne ID </h4>
                        <div class="form-group row">
                            <div class="col-md-12 row">
                                <input class="data_man_checkbox" type="checkbox" name="checkbox_add">
                                <label class="col-md-3 col-form-label">Dodaj jako nowe</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 row">
                                <input class="data_man_checkbox" type="checkbox" name="checkbox_update">
                                <label class="col-md-3 col-form-label">Zaaktualizuj punkty</label>
                            </div>
                        </div>



                        <input type="submit" value="Import .csv" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
@stop
<script type="application/javascript">

</script>
