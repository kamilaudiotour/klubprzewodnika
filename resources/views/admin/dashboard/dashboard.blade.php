@extends('layouts.app')

@section('content')
<div class="container hide">
  <div class="row">
    <div class="col-md-3">
      <a href="{{route('news.index')}}" class="d-link">News</a>
    </div>
    <div class="col-md-3">
      <a href="{{route('objects.index')}}" class="d-link">Objects</a>
    </div>
    <div class="col-md-3">
       <a href="{{route('parking.index')}}" class="d-link">Parking</a>
    </div> 
	<div class="col-md-3">
      <a href="{{route('events.index')}}" class="d-link">Events</a>
    </div>
  </div>
</div>
@stop