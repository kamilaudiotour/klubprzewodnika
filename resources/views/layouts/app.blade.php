<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{--    <title>{{ config('app.name', 'Laravel') }}</title>--}}
    <title>Muzeum Emigracji w Gdyni</title>



    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <!-- Scripts -->
  <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>

    <script src="{{ asset('js/app.js') }}"></script>

  {{--    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>--}}
  {{--    <script src="{{ asset('js/summernote.min.js') }}" ></script>--}}
    <script src="{{ asset('js/datepicker.min.js') }}" ></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>


    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-grid.min.css') }}" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  {{--	<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/summernote.css') }}" rel="stylesheet">
  <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet">
  <script src="https://kit.fontawesome.com/7dfad927b2.js" crossorigin="anonymous"></script>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/css/bootstrap-colorpicker.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/js/bootstrap-colorpicker.js"></script>

      <!-- include summernote css/js -->

</head>
<body @if (Auth::guest()) style="background: #ff9947!important;" @endIf>
    <div id="app">
		@if (!Auth::guest())
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm ">
            <div class="container">
			<div class="hz">

                <a  class="navbar-brand" style="padding: 10px!important;" href="{{route('audio_guide.index')}}"><img class="custom-navbar-logo" src="{{ asset('img/logo.svg') }}"></a>

                <a class="navbar-brand">
                    <div class="custom-user">
                        <ul>
                            <li>Witam,</li>
                            <li>Jesteś zalogowany/-a jako:</li>
                            <li><b>{{Auth::user()->name}}</b></li>
                        </ul>
                    </div>
                </a>


                <a class="navbar-brand" href="{{route('audio_guide.index')}}">AUDIOPRZEWODNIK</a>
                <a class="navbar-brand" href="{{route('code.index')}}">KODY DOSTĘPU</a>
                <a class="navbar-brand" href="{{route('lang.index')}}">JĘZYKI</a>


                <a class="navbar-brand" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Wyłoguj się <i class="fa fa-sign-out-alt"></i></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <!--<ul class="navbar-nav ml-auto">

                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-code dropdown-code-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul> -->
                </div>
            </div>
            </div>
        </nav>
@endif
        <main class="py-4">
            @yield('content')
        </main>
    </div>


	    <script src="{{ asset('js/function.js') }}"></script>
</body>
</html>
