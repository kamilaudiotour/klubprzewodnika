@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>W celu uzyskania dostępu do CMS skontaktuj się z </h2><h2><a href="mailto:slawek@audiotour.pl">slawek@audiotour.pl</a></h2>
        </div>
    </div>
</div>
@endsection
