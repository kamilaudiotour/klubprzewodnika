@extends('layouts.app')

@section('content')
    <div class="lang-wrap container-fluid">
        <div class="row">
            <div style="padding: 0px!important;" class="col-6">
                <img class="header-logo" src="{{ asset('img/logo.svg') }}">
            </div>
            <div style="padding: 0px!important;" class="col-3"> </div>
            <div style="padding: 0px!important;" class="col-3">
                <a href="" data-toggle="modal" data-target="#exampleModal">
                    <img class="header-lang" src="{{ asset('img/lang.svg') }}">
                </a>
            </div>

            <div style="height: 15px;" class="col-12"></div>

            <div class="col-12" style="padding: 0px!important;">
                @foreach($items as $item)
                    @if($item['parent'] == null)
                        <div class="row audio-card" >
                            <div class="col-3" style="padding: 0px!important;">
                                <img style="height: 100%; width: 100%;" src="{{$item['banner']}}" alt="{{$item['name']}}">
                            </div>
                            <div @if($item['is_video'] == 1) url="{{$item['video_file']}}" data-toggle="modal" data-target="#videoModal" @else @endif class="videoModal @if($item['is_child'] == 1) col-8 @else col-9 @endif" style="padding: 0px 10px 0px 20px!important;">
                                <h5 style="margin-left: 5px; margin-top: 15px; margin-bottom: 15px;line-height: 25px;" class="card_title-18">{{$item['name']}}</h5>
                                @if($item['is_video'] == 0)
                                    <div class="player" style="margin-bottom: 15px;">
                                        <audio id="audio{{$item['id']}}" bind="false">
                                            <source src="{{$item['audio_file']}}" type="audio/mpeg">
                                        </audio>
                                        <div class="audioplayer voice">
                                            <div class="stop" stop-id="{{$item['id']}}"></div>
                                            <div class="play-pause voice" id="{{$item['id']}}"></div>
                                            <div class="seekbar-container voice">
                                                <div class="seekbar-bg voice" id="audio{{$item['id']}}seekbar-bg" seekbarID="{{$item['id']}}">
                                                    <div class="seekbar voice" id="audio{{$item['id']}}seekbar" style="width:0%;"></div>
                                                </div>
                                                <div class="currentTime" id="audio{{$item['id']}}currentTime">0:00</div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if($item['is_child'] == 1)
                            <div class="col-1" style="padding: 0px!important;">
                                    <a class="target" id="target" is-open="false" data-target="parent{{$item['id']}}">
                                        <img id="parent{{$item['id']}}img" src="{{ asset('img/down.png') }}">
                                    </a>
                            </div>
                            @endif
                        </div>
                    @elseif($item['parent'] != null)
                        <div @if($item['is_video'] == 1) url="{{$item['video_file']}}" data-toggle="modal" data-target="#videoModal" @else @endif class="videoModal row audio-card-sub parent{{$item['parent']}}">
                            <div class="row">
                                <div class="col-12" style="padding: 0px!important;">
                                    <h5 style="margin: 10px;" class="card_title-15">{{$item['name']}}</h5>
                                </div>
                                @if($item['is_video'] == 0)
                                <div class="col-12" style="padding: 0px!important;">
                                    <div class="player" style="margin-left: 7px;margin-right: 5px; margin-bottom: 10px;" >
                                        <audio id="audio{{$item['id']}}" bind="false">
                                            <source src="{{$item['audio_file']}}" type="audio/mpeg">
                                        </audio>
                                        <div class="audioplayer voice">
                                            <div class="stop" stop-id="{{$item['id']}}"></div>
                                            <div class="play-pause voice" id="{{$item['id']}}"></div>
                                            <div class="seekbar-container voice">
                                                <div class="seekbar-bg voice" id="audio{{$item['id']}}seekbar-bg" seekbarID="{{$item['id']}}">
                                                    <div class="seekbar voice" id="audio{{$item['id']}}seekbar" style="width:0%;"></div>
                                                </div>
                                                <div class="currentTime" id="audio{{$item['id']}}currentTime">0:00</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    @endif
                    <!-- @if($item['is_video'])
                            <div class="modal fade" id="videoModal{{$item['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div style="margin: 0 auto!important; top: 15%!important; padding: 5px;" class="modal-dialog" role="document">
                                    <div style="background: #000!important;" class="modal-content">
                                        <button data-dismiss="modal" class="dialog-close">X</button>
                                        <div class="modal-body">
                                            <video width="100%" height="100%" controls autoplay>
                                                <source src="{{$item['video_file']}}" type="video/mp4">
                                                <source src="{{$item['video_file']}}" type="video/ogg">
                                                Your browser does not support the video tag.
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @else
                    @endif -->
                @endforeach
            </div>
        </div>
    </div>

    {{--modal-video--}}
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div style="margin: 0 auto!important; top: 15%!important; padding: 5px;" class="modal-dialog" role="document">
            <div style="background: #000!important;" class="modal-content">
                <button data-dismiss="modal" class="dialog-close">X</button>
                <div id="mp4" class="modal-body">
                    <!-- <video width="100%" height="100%" controls autoplay> -->
                        <!-- <source id="mp4" src="" type="video/mp4">
                        <source id="ogg" src="" type="video/ogg">
                        Your browser does not support the video tag. -->
                    <!-- </video> -->
                </div>
            </div>
        </div>
    </div>

    {{--modal--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div style="margin: 0 auto!important; top: 30%!important; padding: 10px!important;" class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                  <h3 class="code-title">
                      @if($lang == "pl")
                          Czy na pewno <br> chcesz wybrać nowy język?
                      @elseif($lang == "en")
                          Are you sure you want to select a new language?
                      @elseif($lang == "de")
                          Möchten Sie die neue Sprache wirklich auswählen?
                      @elseif($lang == "fr")
                          Êtes-vous sûr de vouloir sélectionner la nouvelle langue?
                      @elseif($lang == "hi")
                          Está seguro de que desea seleccionar el nuevo idioma?
                      @elseif($lang == "sv")
                          Är du säker på att du vill välja det nya språket?
                      @elseif($lang == "pjm")
                        Czy na pewno <br> chcesz wybrać nowy język?
                      @else
                          Are you sure you want to select a new language?
                      @endif
                  </h3>

                  <div style="margin-top: 30px; margin-bottom: 10px">
                      <div class="row">
                          <div class="col-6 dialog-button">
                              <button data-dismiss="modal" type="button" class="btn btn-dark">
                                  @if($lang == "pl")
                                      NIE
                                  @elseif($lang == "en")
                                      NO
                                  @elseif($lang == "de")
                                      NEIN
                                  @elseif($lang == "fr")
                                      NON
                                  @elseif($lang == "hi")
                                      NO
                                  @elseif($lang == "sv")
                                      NEJ
                                  @elseif($lang == "pjm")
                                      NIE
                                  @else
                                      NO
                                  @endif
                              </button>
                          </div>
                          <div class="col-6 dialog-button">
                              <a href="{{route('audioprzewodnik.index')}}">
                                  <button type="button" class="btn btn-dark">
                                      @if($lang == "pl")
                                          TAK
                                      @elseif($lang == "en")
                                          YES
                                      @elseif($lang == "de")
                                          JA
                                      @elseif($lang == "fr")
                                          OUI
                                      @elseif($lang == "hi")
                                          SI
                                      @elseif($lang == "sv")
                                          JA
                                      @elseif($lang == "pjm")
                                          NIE
                                      @else
                                          TAK
                                      @endif
                                  </button>
                                </a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">


        var lastPlayerID = null
        var currentPlayre = null

        $( '.target' ).click(function() {
            if ($(this).attr("is-open") === "false"){
                var it = $(this).attr("data-target");
                $('.'+it).show();
                $(this).attr("is-open", "true");
                $('#'+it+'img').attr('src', '../img/up.png');
            }else if($(this).attr("is-open") === "true"){
                var it = $(this).attr("data-target");
                $('.'+it).hide();
                $('#'+it+'img').attr('src', '../img/down.png');
                $(this).attr("is-open", "false");
            }
        });

        $('.play-pause').click(function(){
            let player = $('#audio'+$(this).attr('id'));

            if (lastPlayerID != null){
                if ($(this).attr('id') !== lastPlayerID){
                    $('#audio'+lastPlayerID).trigger('pause');
                    let playPause = $('#'+lastPlayerID);
                    playPause.removeClass('playing');
                    playPause.css('background-image', 'url("../img/play.png")');
                }
            }

            if (player.attr("bind") === "false"){
                player.trigger('load');

                $.ajax({
                  type: "GET",
                  url: "../select.php",
                  data: {ids: String($(this).attr('id'))},
                });


                player.bind("timeupdate", function() {
                    $('#'+$(this).attr('id')+'seekbar').stop().animate({width: 100 * (this.currentTime / this.duration) + "%"}, 500);
                    $('#'+$(this).attr('id')+'currentTime').text(getTimeCodeFromNum(this.duration - this.currentTime));
                    player.attr("bind", "true");
                    if ((100 * (this.currentTime / this.duration)) === 100){
                        stopPlayer(lastPlayerID);

                        $.ajax({
                          type: "GET",
                          url: "../was.php",
                          data: {ids: String(lastPlayerID)},
                        });
                    }
                    currentPlayre = this
                });
            }

            if($(this).hasClass('playing')){
                console.log("playing")
                player.trigger('pause');
                $(this).removeClass('playing');
                $('.play-pause#'+$(this).attr('id')).css('background-image', 'url("../img/play.png")');
            } else{
                console.log("slawplaying")
                player.trigger('play');
                $(this).addClass('playing');
                $('.play-pause#'+$(this).attr('id')).css('background-image', 'url("../img/pause.png")');
            }

            lastPlayerID = $(this).attr('id')
        });

        $('.stop').click(function (){
            stopPlayer($(this).attr("stop-id"));
        });

        function stopPlayer(stopID){
            let player = $('#audio' + stopID);
            player.trigger('pause');
            player.trigger('load');
            player.currentTime = 0;
            let playPause = $('#'+stopID)
            playPause.removeClass('playing');
            playPause.css('background-image', 'url("../img/play.png")');
            $('#audio' + stopID +'seekbar').css({width: 0}, 500);
            $('#audio' + stopID +'currentTime').text("0:00");
            player.attr("bind", "false");
        }

        function getTimeCodeFromNum(num) {
            let seconds = parseInt(num);
            let minutes = parseInt(seconds / 60);
            seconds -= minutes * 60;
            const hours = parseInt(minutes / 60);
            minutes -= hours * 60;

            if (String(hours).padStart(2, 0) === "NaN") return "0:00"

            if (hours === 0) return `${minutes}:${String(seconds % 60).padStart(2, 0)}`;
            return `${String(hours).padStart(2, 0)}:${minutes}:${String(seconds % 60).padStart(2, 0)}`;
        }


        $('.videoModal').click(function (){
            // $('#mp4').attr("src", "http://guide.polska1.pl/"+$(this).attr("url"));
            // $('#ogg').attr("src", "http://guide.polska1.pl/"+$(this).attr("url"));
            $('video').html("")
            let a = "http://guide.polska1.pl/"+$(this).attr("url");
            $('#mp4').html("<video width='100%' height='100%' controls autoplay><source src='"+a+"' type='video/mp4'></video>")

            // $('video').play();
        });

        $('.modal').click(function (){
            $('video').each(function (){
                $(this).trigger('pause');
            });
        });

        $('.dialog-close').click(function (){
            $('video').each(function (){
                $(this).trigger('pause');
            });
        });


        $('.seekbar-bg').click(function (){
            let id = $(this).attr('seekbarID');
            if (lastPlayerID != null && lastPlayerID === id){
                // Get div offset position (top and left)
                var offset = $(this).offset();
                // Get the full width of the seek bar
                var totalWidth = $(this).outerWidth();
                // Get seek bar click position
                var left = event.pageX - offset.left;
                // Get the track total duration
                var totalDuration = currentPlayre.duration;
                // Get the current time position
                currentPlayre.currentTime = (left * totalDuration) / totalWidth
            }
        });
    </script>
@endsection
