@extends('layouts.app')

@section('content')
    <div class="lang-wrap container-fluid">
        <div class="row">
            <div style="padding: 0px!important;" class="col-6">
                <img class="header-logo" src="{{ asset('img/logo.svg') }}">
            </div>
            <div style="padding: 0px!important;" class="col-3"> </div>
            <div style="padding: 0px!important;" class="col-3">
                <a href="" data-toggle="modal" data-target="#exampleModal">
                    <img class="header-lang" src="{{ asset('img/lang.svg') }}">
                </a>
            </div>
            <div class="col-12">
                <h3 class="code-title">
                    @if($lang == "pl")
                        Wpisz kod
                    @elseif($lang == "en")
                        Enter code
                    @elseif($lang == "de")
                        Code eingeben
                    @elseif($lang == "fr")
                        Entrez le code
                    @elseif($lang == "hi")
                        Introduzca el código
                    @elseif($lang == "sv")
                        Ange kod
                    @elseif($lang == "pjm")
                        Wpisz kod
                    @else
                        Enter code
                    @endif</h3>
            </div>
            <div class="col-12" style="margin-bottom: 20px; margin-top: 10px;">
                <h3 id="input" class="code-input">......</h3>
            </div>

            {{--     1 row       --}}
            <div class="col-4 number-button"><button id="nr1" type="button" class="btn btn-light">1</button></div>
            <div class="col-4 number-button"><button id="nr2" type="button" class="btn btn-light">2</button></div>
            <div class="col-4 number-button"><button id="nr3" type="button" class="btn btn-light">3</button></div>
            {{--     2 row       --}}
            <div class="col-4 number-button"><button id="nr4" type="button" class="btn btn-light">4</button></div>
            <div class="col-4 number-button"><button id="nr5" type="button" class="btn btn-light">5</button></div>
            <div class="col-4 number-button"><button id="nr6" type="button" class="btn btn-light">6</button></div>
            {{--     3 row       --}}
            <div class="col-4 number-button"><button id="nr7" type="button" class="btn btn-light">7</button></div>
            <div class="col-4 number-button"><button id="nr8" type="button" class="btn btn-light">8</button></div>
            <div class="col-4 number-button"><button id="nr9" type="button" class="btn btn-light">9</button></div>
            {{--     4 row       --}}
            <div class="col-4 number-button-black"><button id="backspase" style="padding-bottom: 10px; padding-right: 14px;" type="button" class="btn btn-dark">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-backspace-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M15.683 3a2 2 0 0 0-2-2h-7.08a2 2 0 0 0-1.519.698L.241 7.35a1 1 0 0 0 0 1.302l4.843 5.65A2 2 0 0 0 6.603 15h7.08a2 2 0 0 0 2-2V3zM5.829 5.854a.5.5 0 1 1 .707-.708l2.147 2.147 2.146-2.147a.5.5 0 1 1 .707.708L9.39 8l2.146 2.146a.5.5 0 0 1-.707.708L8.683 8.707l-2.147 2.147a.5.5 0 0 1-.707-.708L7.976 8 5.829 5.854z"/>
                    </svg>
                </button></div>
            <div class="col-4 number-button"><button id="nr0" type="button" class="btn btn-light">0</button></div>
            <div class="col-4 number-button-black">
                <form style="height: 51px;" action="{{route('audioprzewodnik.update', $id)}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT" />
                    {{csrf_field()}}
                    <input type="hidden" id="code" name="code" value="" />
                    <button type="submit" class="btn btn-dark">OK</button>
                </form>
            </div>


        </div>
    </div>

    <div class="col-12 rodo">
        <h5>
            @if($lang == "pl")
                Używamy plików cookies, aby ułatwić Ci kożystanie z naszego serwisu oraz do celów statystycznych. Jeśli nie blokujesz tych plików, to zgadzasz się na ich użycie oraz zapisanie w pamięci urządzenia. Pamiętaj że możesz samodzielnie zarządzać
            @elseif($lang == "en")
                We use cookies to facilitate the use of our website and for statistical purposes. If you are not blocking these files, you agree to their use and saving in the device memory. Remember that you can manage yourself
            @elseif($lang == "de")
                Wir verwenden Cookies, um die Nutzung unserer Website zu erleichtern und zu statistischen Zwecken. Wenn Sie diese Dateien nicht blockieren, stimmen Sie ihrer Verwendung und Speicherung im Gerätespeicher zu. Denken Sie daran, dass Sie sich selbst verwalten können
            @elseif($lang == "fr")
                Nous utilisons des cookies pour faciliter l'utilisation de notre site Web et à des fins statistiques. Si vous ne bloquez pas ces fichiers, vous acceptez leur utilisation et leur sauvegarde dans la mémoire de l'appareil. N'oubliez pas que vous pouvez vous gérer
            @elseif($lang == "hi")
                Utilizamos cookies para facilitar el uso de nuestro sitio web y con fines estadísticos. Si no bloquea estos archivos, acepta su uso y almacenamiento en la memoria del dispositivo. Recuerda que puedes manejarte solo
            @elseif($lang == "sv")
                Vi använder cookies för att underlätta användningen av vår webbplats och för statistiska ändamål. Om du inte blockerar dessa filer godkänner du att de används och sparas i enhetens minne. Kom ihåg att du kan klara dig själv
            @elseif($lang == "pjm")
              Używamy plików cookies, aby ułatwić Ci kożystanie z naszego serwisu oraz do celów statystycznych. Jeśli nie blokujesz tych plików, to zgadzasz się na ich użycie oraz zapisanie w pamięci urządzenia. Pamiętaj że możesz samodzielnie zarządzać
            @else
                We use cookies to facilitate the use of our website and for statistical purposes. If you are not blocking these files, you agree to their use and saving in the device memory. Remember that you can manage yourself
            @endif
        </h5>
        <button id="rodo-btn">
            @if($lang == "pl")
                Akceptuj
            @elseif($lang == "en")
                Accept
            @elseif($lang == "de")
                Akzeptieren
            @elseif($lang == "fr")
                J'accepte
            @elseif($lang == "hi")
                Aceptar
            @elseif($lang == "sv")
                Acceptera
            @elseif($lang == "pjm")
              Akceptuj
            @else
                Accept
            @endif
        </button>
    </div>
{{--modal--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div style="margin: 0 auto!important; top: 30%!important; padding: 10px!important;" class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h3 class="code-title">
                        @if($lang == "pl")
                            Czy na pewno <br> chcesz wybrać nowy język?
                        @elseif($lang == "en")
                            Are you sure you want to select a new language?
                        @elseif($lang == "de")
                            Möchten Sie die neue Sprache wirklich auswählen?
                        @elseif($lang == "fr")
                            Êtes-vous sûr de vouloir sélectionner la nouvelle langue?
                        @elseif($lang == "hi")
                            Está seguro de que desea seleccionar el nuevo idioma?
                        @elseif($lang == "sv")
                            Är du säker på att du vill välja det nya språket?
                        @elseif($lang == "pjm")
                          Czy na pewno <br> chcesz wybrać nowy język?
                        @else
                            Are you sure you want to select a new language?
                        @endif
                    </h3>

                    <div style="margin-top: 30px; margin-bottom: 10px">
                        <div class="row">
                            <div class="col-6 dialog-button">
                                <button data-dismiss="modal" type="button" class="btn btn-dark">
                                    @if($lang == "pl")
                                        NIE
                                    @elseif($lang == "en")
                                        NO
                                    @elseif($lang == "de")
                                        NEIN
                                    @elseif($lang == "fr")
                                        NON
                                    @elseif($lang == "hi")
                                        NO
                                    @elseif($lang == "sv")
                                        NEJ
                                    @elseif($lang == "pjm")
                                        NIE
                                    @else
                                        NO
                                    @endif
                                </button>
                            </div>
                            <div class="col-6 dialog-button">
                                <a href="{{route('audioprzewodnik.index')}}">
                                    <button type="button" class="btn btn-dark">
                                        @if($lang == "pl")
                                            TAK
                                        @elseif($lang == "en")
                                            YES
                                        @elseif($lang == "de")
                                            JA
                                        @elseif($lang == "fr")
                                            OUI
                                        @elseif($lang == "hi")
                                            SI
                                        @elseif($lang == "sv")
                                            JA
                                        @elseif($lang == "pjm")
                                            NIE
                                        @else
                                            TAK
                                        @endif
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $( "#nr0" ).click(function() {
            addToLable("0")
        });
        $( "#nr1" ).click(function() {
            addToLable("1")
        });
        $( "#nr2" ).click(function() {
            addToLable("2")
        });
        $( "#nr3" ).click(function() {
            addToLable("3")
        });
        $( "#nr4" ).click(function() {
            addToLable("4")
        });
        $( "#nr5" ).click(function() {
            addToLable("5")
        });
        $( "#nr6" ).click(function() {
            addToLable("6")
        });
        $( "#nr7" ).click(function() {
            addToLable("7")
        });
        $( "#nr8" ).click(function() {
            addToLable("8")
        });
        $( "#nr9" ).click(function() {
            addToLable("9")
        });

        $( "#backspase" ).click(function() {
            deleteFromLable()
        });

        function addToLable(str) {
            var text = $("#input").text()
            if (text == "......"){
                $("#input").text("")
                $( "#input" ).text(str)
                $("#code").val(str)
            }else {
                if (text.length != 6){
                    $( "#input" ).text(text + str)
                    $("#code").val(text + str)
                }
            }
        }

        function deleteFromLable() {
            var text = $("#input").text()
            if (text != "......"){
                var slice = text.slice(0,-1)
                if (slice == ""){
                    slice = "......"
                }
                $( "#input" ).text(slice)
                $("#code").val(slice)
            }
        }

        if (localStorage.getItem('rodo') == "true"){
            $('.rodo').hide();
        }

        $( "#rodo-btn" ).click(function() {
            localStorage.setItem('rodo', 'true')
            $('.rodo').hide();
        });
    </script>
@endsection
