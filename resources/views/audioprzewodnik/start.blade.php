@extends('layouts.app')

@section('content')
    <div style="background: white; z-index: 99" id="preloader">
        <img  class="home-image" src="{{ asset('img/logo.svg') }}">
    </div>
    <div class="lang-wrap container-fluid">
        <div class="row">
            <div style="padding: 0px!important;" class="col-6">
                <img class="header-logo" src="{{ asset('img/logo.svg') }}">
            </div>
            <div class="col-12" style="margin-top: 50px">
                <h3 class="lang-title">Wybierz język</h3>
            </div>
            <div class="col-2"></div>
            <div class="col-8" style="margin-top: 10px">
                <ul class="lang-list-wrap">
                    @foreach($items as $item)
                        <a href="{{route('audioprzewodnik.show', $item->id)}}">
                            <li style="display: flex">
                                <img src="{{$item->flag}}" alt="{{$item->name}}">
                                <h4 @if(strlen($item->name) > 10) style="margin-top: 2px!important;" @endif>{{$item->name}}</h4>
                            </li>
                        </a>
                    @endforeach
                </ul>
            </div>
            <div class="col-2"></div>
        </div>
        <div class="col-12 rodo">
            <h5>
                Używamy plików cookies, aby ułatwić Ci kożystanie z naszego serwisu oraz do celów statystycznych. Jeśli nie blokujesz tych plików, to zgadzasz się na ich użycie oraz zapisanie w pamięci urządzenia. Pamiętaj że możesz samodzielnie zarządzać
            </h5>
            <button id="rodo-btn">Akceptuj</button>
        </div>
    </div>


<script type="text/javascript">
    console.log("test1")
    setTimeout(function() {
        console.log("test")
        $('#preloader').fadeOut();
    }, 1000);

    if (localStorage.getItem('rodo') == "true"){
        $('.rodo').hide();
    }

    $( "#rodo-btn" ).click(function() {
        localStorage.setItem('rodo', 'true')
        $('.rodo').hide();
    });
</script>
@endsection
